from modeltranslation.translator import register, TranslationOptions
from .models import SuccessfulStudents, Faq, MainSlider, TopMenu, MiddleMenu, Pages, FooterMenu


@register(TopMenu)
class TopMenuTranslationOptions(TranslationOptions):
    fields = ('title',)

@register(MiddleMenu)
class MiddleMenuTranslationOptions(TranslationOptions):
    fields = ('title', )

@register(SuccessfulStudents)
class SuccessfulStudentsTranslationOptions(TranslationOptions):
    fields = ('full_name', 'age', 'occupation', 'comment')

@register(Faq)
class FaqTranslationOptions(TranslationOptions):
    fields = ('question', 'answer')

@register(MainSlider)
class MainSliderTranslationOptions(TranslationOptions):
    fields = ('headline', )

@register(Pages)
class PagesTranslationOptions(TranslationOptions):
    fields = ('title', 'keywords', 'description', 'text')

@register(FooterMenu)
class FooterMenuTranslationOptions(TranslationOptions):
    fields = ('title', )
