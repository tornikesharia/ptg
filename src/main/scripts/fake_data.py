from faker import Faker

from main.models import Book, Book2


def run():
    fake = Faker()
    Book2.objects.bulk_create([Book2(
        author=fake.name(),
        title=fake.sentence(),
        published_at=fake.date()
    ) for book in range(1000)])

    # SuccessfulStudents3.objects.bulk_create([SuccessfulStudents3(
    #     author=fake.name(),
    #     title=fake.sentence(),
    #     published_at=fake.date()
    # ) for book in range(1000)])