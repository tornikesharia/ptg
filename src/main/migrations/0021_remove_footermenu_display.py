# Generated by Django 3.1.5 on 2021-04-27 16:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0020_footermenu_display'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='footermenu',
            name='display',
        ),
    ]
