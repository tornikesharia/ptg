# Generated by Django 3.1.5 on 2021-03-16 19:45

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Faq',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.CharField(default='', max_length=200)),
                ('question_en', models.CharField(default='', max_length=200, null=True)),
                ('question_ka', models.CharField(default='', max_length=200, null=True)),
                ('question_ru', models.CharField(default='', max_length=200, null=True)),
                ('answer', models.TextField(blank=True, default='', max_length=2000)),
                ('answer_en', models.TextField(blank=True, default='', max_length=2000, null=True)),
                ('answer_ka', models.TextField(blank=True, default='', max_length=2000, null=True)),
                ('answer_ru', models.TextField(blank=True, default='', max_length=2000, null=True)),
            ],
            options={
                'verbose_name': 'FAQ',
                'verbose_name_plural': 'FAQ',
            },
        ),
        migrations.CreateModel(
            name='MainSlider',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image_desctop', models.ImageField(default='', upload_to='main_slider/')),
                ('image_mobile', models.ImageField(default='', upload_to='main_slider/')),
                ('headline', models.CharField(blank=True, default='', max_length=500)),
                ('headline_en', models.CharField(blank=True, default='', max_length=500, null=True)),
                ('headline_ka', models.CharField(blank=True, default='', max_length=500, null=True)),
                ('headline_ru', models.CharField(blank=True, default='', max_length=500, null=True)),
                ('link', models.CharField(blank=True, default='', max_length=500)),
                ('active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='SuccessfulStudents',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(default='', upload_to='successful_students/')),
                ('full_name', models.CharField(default='', max_length=100)),
                ('full_name_en', models.CharField(default='', max_length=100, null=True)),
                ('full_name_ka', models.CharField(default='', max_length=100, null=True)),
                ('full_name_ru', models.CharField(default='', max_length=100, null=True)),
                ('slug', models.SlugField(default='', max_length=100, unique=True)),
                ('age', models.CharField(default='', max_length=100)),
                ('age_en', models.CharField(default='', max_length=100, null=True)),
                ('age_ka', models.CharField(default='', max_length=100, null=True)),
                ('age_ru', models.CharField(default='', max_length=100, null=True)),
                ('occupation', models.CharField(blank=True, default='', max_length=100)),
                ('occupation_en', models.CharField(blank=True, default='', max_length=100, null=True)),
                ('occupation_ka', models.CharField(blank=True, default='', max_length=100, null=True)),
                ('occupation_ru', models.CharField(blank=True, default='', max_length=100, null=True)),
                ('test_name', models.CharField(default='', max_length=100)),
                ('scores', models.CharField(blank=True, default='', max_length=100)),
                ('comment', models.TextField(blank=True, default='', max_length=2000)),
                ('comment_en', models.TextField(blank=True, default='', max_length=2000, null=True)),
                ('comment_ka', models.TextField(blank=True, default='', max_length=2000, null=True)),
                ('comment_ru', models.TextField(blank=True, default='', max_length=2000, null=True)),
                ('active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'Successful student',
                'verbose_name_plural': 'Successful students',
            },
        ),
    ]
