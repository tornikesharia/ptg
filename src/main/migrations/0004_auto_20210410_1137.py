# Generated by Django 3.1.5 on 2021-04-10 08:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20210410_1124'),
    ]

    operations = [
        migrations.AlterField(
            model_name='topmenu',
            name='text',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='topmenu',
            name='text_en',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AlterField(
            model_name='topmenu',
            name='text_ka',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AlterField(
            model_name='topmenu',
            name='text_ru',
            field=models.TextField(blank=True, default='', null=True),
        ),
    ]
