# Generated by Django 3.1.5 on 2021-04-25 16:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0009_auto_20210416_2200'),
    ]

    operations = [
        migrations.CreateModel(
            name='Common',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50, unique=True)),
                ('image', models.ImageField(default='', upload_to='common/')),
                ('slug', models.SlugField(blank=True, default='', max_length=100, null=True, unique=True)),
                ('keywords', models.CharField(max_length=500)),
                ('description', models.CharField(max_length=500)),
                ('text', models.TextField(blank=True, default='')),
                ('active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'Common',
                'verbose_name_plural': 'Common',
            },
        ),
    ]
