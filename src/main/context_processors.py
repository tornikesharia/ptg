from .utils import get_host_name, get_site_full_path
from .models import TopMenu, MiddleMenu, FooterMenu, MainSlider, SuccessfulStudents
from pprint import pprint


def get_host_name_tag(request):
    return {'HOST_NAME': get_host_name(request) }

def get_site_full_path_tag(request):
    return {'SITE_FULL_PATH': get_site_full_path(request)}   

def get_top_menu(request):
    top_menu = TopMenu.objects.all()
    return {
        'top_menu': top_menu
    }

def get_middle_menu(request):
    middle_menu = MiddleMenu.objects.all()
    return {
        'middle_menu': middle_menu
    }

def get_footer_menu(request):
    footer_menu = FooterMenu.objects.all()
    return {
        'footer_menu': footer_menu
    }

def get_main_slider(request):
    main_slider_items = MainSlider.objects.all()
    return {
        'main_slider_items': {
            'items': main_slider_items
        }
    }

def get_successfull_students(request):
    slider_title = SuccessfulStudents._meta.verbose_name_plural.title()
    items = SuccessfulStudents.objects.filter(active=True)
    return {
            'successful_students': {
                'title': slider_title,
                'items': items
        },
    }
