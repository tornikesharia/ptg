def get_host_name(request):
    PROTOCOL = request.scheme + '://'    
    HOST = request.get_host() 
    return PROTOCOL + HOST

def get_site_full_path(request):
    PROTOCOL = request.scheme + '://'    
    HOST = request.get_host()
    URI = request.get_full_path() 
    return PROTOCOL + HOST + URI