from django.db import models
import mptt
from mptt.models import MPTTModel, TreeForeignKey
from django.utils.html import mark_safe
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _


class Pages(models.Model):
    title = models.CharField(max_length=50, unique=False)
    image = models.ImageField(upload_to='common/', default='', blank=True)
    slug = models.SlugField(max_length=100, unique=True, default='', null=True, blank=True)
    keywords = models.CharField(max_length=500, default='', blank=True)
    description = models.CharField(max_length=500, default='', blank=True)
    text = models.TextField(blank=True, default='')
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)

        super(Pages, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _('Page')
        verbose_name_plural = 'Pages'

    def __str__(self):
        return self.title

class TopMenu(MPTTModel):
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    title = models.CharField(max_length=50, unique=True)
    link = models.CharField(max_length=1000, default='', blank=True)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)

        super(TopMenu, self).save(*args, **kwargs)
    class Meta:
        verbose_name = _('Top Menu')
        verbose_name_plural = _('Top Menu')  


    class MPTTMeta:
        order_insertion_by = ['title']      

    def __str__(self):
        return self.title

mptt.register(TopMenu, order_insertion_by=['title'])     

class MiddleMenu(MPTTModel):
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    title = models.CharField(max_length=50, unique=False)
    link = models.CharField(max_length=1000, default='', blank=True)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Middle Menu(Links)')
        verbose_name_plural = _('Middle Menu(Links)')

    class MPTTMeta:
        order_insertion_by = ['title']

    def __str__(self):
        return self.title

mptt.register(MiddleMenu, order_insertion_by=['title'])    

class MainSlider(models.Model):
    image_desctop = models.ImageField(upload_to='main_slider/', default='')
    image_mobile = models.ImageField(upload_to='main_slider/', default='')
    headline = models.CharField(max_length=500, blank=True, default='')
    link = models.CharField(max_length=500, blank=True, default='')
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.headline


class SuccessfulStudents(models.Model):
    image = models.ImageField(upload_to='successful_students/', default='')
    full_name = models.CharField(max_length=100, blank=False, default='')
    slug = models.SlugField(max_length=100, unique=True, default='', null=True, blank=True)
    age = models.CharField(max_length=100, default='')
    occupation = models.CharField(max_length=100, blank=True, default='')
    test_name = models.CharField(max_length=100, blank=False, default='')
    scores = models.CharField(max_length=100, blank=True, default='')
    comment = models.TextField(max_length=2000, blank=True, default='')
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Successful student')
        verbose_name_plural = _('Successful students')

    def __str__(self):
        return self.full_name

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.full_name)

        super(SuccessfulStudents, self).save(*args, **kwargs)        

    # def image_tag(self):
    #     from django.utils.html import mark_safe
    #     return mark_safe('<a href="{url}"><img src="{url}" style="width: 70px" alt="" /></a>'.format(url = self.image.url))
    # image_tag.short_description = 'image'        


class Faq(models.Model):
    question = models.CharField(max_length=200, blank=False, default='')
    answer = models.TextField(max_length=2000, blank=True, default='')

    class Meta:
        verbose_name = _('FAQ')
        verbose_name_plural = 'FAQ'

    def __str__(self):
        return self.question


class FooterMenu(MPTTModel):
    CHOICES = (
        ('_blank', '_blank'),
    )
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    title = models.CharField(max_length=50, unique=False)
    link = models.CharField(max_length=1000, default='', blank=True)
    link_state = models.CharField(max_length=6, choices=CHOICES, blank=True)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Footer Menu(Links)')
        verbose_name_plural = _('Footer Menu(Links)')

    class MPTTMeta:
        order_insertion_by = ['title']

    def __str__(self):
        return self.title

mptt.register(FooterMenu, order_insertion_by=['title'])    



     