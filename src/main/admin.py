from django.contrib import admin
from modeltranslation.admin import TranslationAdmin
from mptt.admin import DraggableMPTTAdmin
from django_summernote.admin import SummernoteModelAdmin
from .models import SuccessfulStudents, Faq, MainSlider, TopMenu, MiddleMenu, Pages, FooterMenu
from django.utils.html import mark_safe

class SuccessfulStudentsAdmin(SummernoteModelAdmin):
    list_display = ('id', 'thumb', 'full_name', 'slug', 'active')
    list_editable = ['active']

    summernote_fields = '__all__'

    readonly_fields = ["thumb"]

    def thumb(self, obj):
        return mark_safe('<img src="{url}" style="width: {width}" />'.format(
            url = obj.image.url,
            width="70px"
        )
    )

class MainSliderAdmin(admin.ModelAdmin):
    list_display = ('id', 'thumb', 'headline', 'active')
    list_editable = ['active']

    readonly_fields = ["thumb"]

    def thumb(self, obj):
        return mark_safe('<img src="{url}" style="width: {width}" />'.format(
            url = obj.image_desctop.url,
            width="70px"
        )
    )

admin.site.register(TopMenu, DraggableMPTTAdmin,
    list_display=(
        'tree_actions',
        'indented_title',
        'tree_id',
        'active',
        'id',
    ),
    list_editable = ['active'],
    list_display_links=(
        'indented_title',
    ), 
    mptt_level_indent = 40,
)

admin.site.register(MiddleMenu, DraggableMPTTAdmin,
    list_display=(
        'tree_actions',
        'indented_title',
        'tree_id',
        'active',
        'id',
    ),
    list_editable = ['active'],
    list_display_links=(
        'indented_title',
    ), 
    mptt_level_indent = 40
)

admin.site.register(FooterMenu, DraggableMPTTAdmin,
    list_display=(
        'tree_actions',
        'indented_title',
        'tree_id',
        'active',
        'id',
    ),
    list_editable = ['active'],
    list_display_links=(
        'indented_title',
    ), 
    mptt_level_indent = 40
)

class FaqAdmin(SummernoteModelAdmin):
    summernote_fields = '__all__'

class PagesAdmin(SummernoteModelAdmin):
    list_display = ('id', 'title', 'slug', 'active')
    list_editable = ['active']
    search_fields = ['title','slug']
    summernote_fields = '__all__'

admin.site.register(SuccessfulStudents, SuccessfulStudentsAdmin)
admin.site.register(Faq, FaqAdmin)
admin.site.register(MainSlider, MainSliderAdmin)
admin.site.register(Pages, PagesAdmin)



