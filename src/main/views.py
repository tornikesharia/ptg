from django.shortcuts import render, get_list_or_404, get_object_or_404
from django.views.generic import (ListView, DetailView)
from django.utils.translation import gettext as _
from .models import SuccessfulStudents, Faq, MainSlider, TopMenu, MiddleMenu, Pages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from itertools import chain
from django.db.models import Q
from .utils import get_host_name
from pprint import pprint


def index(request):
    item = get_object_or_404(Pages, slug='home', active=True)
    if item.image and item.image.url:
        host_name = get_host_name(request)
        item.image_full_path = host_name + item.image.url

    return render(request, 'pages.html', {
        'page': item,
    })

def pages(request, slug):
    context = {}
    page_item = get_object_or_404(Pages, slug=slug, active=True)
    context['page'] = page_item

    if slug == 'search':
        search_query = request.GET.get('q', '')
        if search_query:
            pages_lookup = (Q(title__icontains=search_query) | Q(text__icontains=search_query))
            students_lookup = (Q(full_name__icontains=search_query) | Q(comment__icontains=search_query))
            page_items = Pages.objects.filter(pages_lookup)
            students_items = SuccessfulStudents.objects.filter(students_lookup)

            queryset_chain = chain(
                page_items,
                students_items
            )  
            search_items = sorted(queryset_chain, 
                        key=lambda instance: instance.pk, 
                        reverse=True)
            
            p = request.GET.get('p')
            paginator = Paginator(search_items, 2)

            try:
                search_items = paginator.page(p)
            except PageNotAnInteger:
                search_items = paginator.page(1)
            except EmptyPage:
                search_items = paginator.page(paginator.num_pages)
                
            pprint(search_items)

            if search_items:
                context['search_items'] = search_items
            else:
                context['search_items'] = []
    if slug == 'faq':
        items = Faq.objects.all()
        context['faq_items'] = items
    if slug == 'successful-students':
        items = SuccessfulStudents.objects.filter(active=True)
        p = request.GET.get('p')
        paginator = Paginator(items, 5)

        try:
            items = paginator.page(p)
        except PageNotAnInteger:
            items = paginator.page(1)
        except EmptyPage:
            items = paginator.page(paginator.num_pages)

        context['successful_students_items'] = items

    return render(request, 'pages.html', context)

def successful_students_detail(request, slug):
    item = get_object_or_404(SuccessfulStudents, slug=slug, active=True)
    if item.image.url:
        host_name = get_host_name(request)
        item.image_full_path = host_name + item.image.url
    return render(request, 'successful-students-detail.html', {
        'item': item
    })
