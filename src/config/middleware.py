from config.settings import LANGUAGE_COOKIE_NAME
from django.utils import translation


def simple_middleware(get_response):
    # One-time configuration and initialization.

    def middleware(request):
        # Change here!! this code will be executed for all requests

        # Code to be executed for each request before
        # the view (and later middleware) are called.

        response = get_response(request)

        request_language = translation.get_language()
        cookie_lang_value = request.COOKIES.get(LANGUAGE_COOKIE_NAME)  # gets language cookie

        if request.method == 'GET':
            # if get new language request and it isn't equal to existing cookie then set new language
            if request_language != cookie_lang_value:
                response.set_cookie(LANGUAGE_COOKIE_NAME, request_language, max_age=63072000)

        # Code to be executed for each request/response after
        # the view is called.

        return response

    return middleware