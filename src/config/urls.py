from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from main import views

admin.site.site_header = _('PTG Admin')

urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
    path('summernote/', include('django_summernote.urls')),
]

urlpatterns += i18n_patterns(
    path('admin/', admin.site.urls),

    path('', views.index, name='index'),
    path('<slug:slug>/', views.pages, name='pages'),
    path('successful-students/<slug:slug>/', views.successful_students_detail, name='successful-students-detail'),
    path('search/', views.pages, name='search'),
    prefix_default_language=True
)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)

