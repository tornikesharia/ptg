// Bootstrap
import 'bootstrap'

// jQuery
import jQuery from "jquery"
window.jQuery = window.$ = jQuery

// Swiper
import Swiper, { Autoplay, Navigation, Pagination, A11y, EffectFade, Parallax, Lazy } from 'swiper'
import 'swiper/swiper-bundle.css'

// Fancybox
import '@fancyapps/fancybox'
import '@fancyapps/fancybox/dist/jquery.fancybox.min.css'

import ddsmoothmenu from './plugins/ddsmoothmenu'
ddsmoothmenu.init({
    mainmenuid: "smoothmenu2", //Menu DIV id
    orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
    classname: 'ddsmoothmenu2', //class added to menu's outer DIV
    //customtheme: ["#804000", "#482400"],
    contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
ddsmoothmenu.init({
    mainmenuid: "smoothmenu1", //menu DIV id
    orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
    classname: 'ddsmoothmenu', //class added to menu's outer DIV
    //customtheme: ["#1c5a80", "#18374a"],
    contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

// configure Swiper to use modules
Swiper.use([Autoplay, Navigation, Pagination, A11y, EffectFade, Parallax, Lazy])

$(function(){
    new Swiper('.main-slider', {
        slidesPerView: 'auto',
        autoplay: {
            delay: 30000,
        },
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            dynamicBullets: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        lazy: true,
        parallax: true,
        slidesPerView: 1
    })
    
    new Swiper('.students-slider__container', {
        autoplay: {
            delay: 5000,
        },
        loop: true,
        slidesPerView: 1,
        spaceBetween: 0,
        navigation: {
            nextEl: '.students-slider__next',
            prevEl: '.students-slider__prev',
        },
        breakpoints: {
            // when window width is >= 220
            220: {
                slidesPerView: 1,
            },
            // when window width is >= 640px
            992: {
                slidesPerView: 2,
            }
        }
    })
    // Menu overlay
    if($('.main__overlay').length === 0) {
        $('.main').prepend('<div class="main__overlay"></div>')
    }
    $(document).on('click', '.js-humberger-left', function(){
        $('.js-humberger-right').removeClass('is-active')
        $('.ddsmoothmenu__bottom-mob, .main__overlay').removeClass('active')
        $(this).toggleClass('is-active')

        if($(this).hasClass('is-active')){
            $('.ddsmoothmenu__top-mob, .main__overlay').addClass('active')
        }else{
            $('.ddsmoothmenu__top-mob, .main__overlay').removeClass('active')
        }
    })
    $(document).on('click', '.js-humberger-right', function(){
        $('.js-humberger-left').removeClass('is-active')
        $('.ddsmoothmenu__top-mob, .main__overlay').removeClass('active')
        $(this).toggleClass('is-active')
        if($(this).hasClass('is-active')){
            $('.ddsmoothmenu__bottom-mob, .main__overlay').addClass('active')
        }else{
            $('.ddsmoothmenu__bottom-mob, .main__overlay').removeClass('active')
        }
    })
    
    // mobile menu
    $('.submenu').hide()
    $('.has-submenu-js').click(function(e) {
        e.preventDefault()
        $(this).next().slideToggle('fast')
    })

    // switch language
    $(document).on('click', '.header__lang-switch-link-js', function(e) {
        e.preventDefault()
        var active_lang = $(this).attr('href')
        $('#lang_switch').find('input[name="language"]').val(active_lang)
        $('#lang_switch').trigger("submit")
    });
})
