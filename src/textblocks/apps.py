from django.apps import AppConfig


class TextBlocksConfig(AppConfig):
    name = 'textblocks'
